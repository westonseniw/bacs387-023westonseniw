﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Message_Cleaner
{
    public class BannedWords
    {


        private string [] _InputBanned;
        public string [] InputBanned
        {
            get { return this._InputBanned; }
            set { this._InputBanned=value; }
        }

        public string[] Splitter(string Banned)
        {
            
            string[] SplitBanned = Banned.Split(' ');
            _InputBanned = SplitBanned;
            return _InputBanned;
        }

        
    }
}
