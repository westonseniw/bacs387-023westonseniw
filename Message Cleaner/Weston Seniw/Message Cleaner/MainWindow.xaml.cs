﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Message_Cleaner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml

    /// </summary>

        

    

    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TbOriginalMessage_GotFocus(object sender, RoutedEventArgs e)
        {
            TbOriginalMessage.Text = null;
        }

        private void TbBannedWords_GotFocus(object sender, RoutedEventArgs e)
        {
            TbBannedWords.Text = null;
        }

        private void BtnClean_Click(object sender, RoutedEventArgs e)
        {
            TbCleanMessage.Text = null;
            string ban = TbBannedWords.Text.ToLower();
            string original = TbOriginalMessage.Text.ToLower();
            BannedWords banWords = new BannedWords();
            OriginalMessage originalIn = new OriginalMessage();
            Cleaner finish = new Cleaner();
            banWords.Splitter(ban);
            originalIn.Splitter(original);
            finish.Clean(banWords.InputBanned, originalIn.InputMessage);
            TbCleanMessage.Text=string.Join(" ",finish.TheCleaner);



        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            TbBannedWords.Text = null;
            TbOriginalMessage.Text = null;
            TbCleanMessage.Text = null;
            TbOriginalMessage.Focus();
        }
    }
}
