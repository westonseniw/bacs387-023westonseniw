﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Message_Cleaner
{
    class OriginalMessage
    {
        
        private string[] _InputMessage;
        public string[] InputMessage
        {
            get { return this._InputMessage; }
            set { this._InputMessage = value; }
        }

        public string[] Splitter(string original)
        {

            string[] SplitBanned = original.Split(' ');
            _InputMessage = SplitBanned;
            return _InputMessage;
        }

    }
}
